# one-page-responsive

One page website with HTML, CSS, javascript, and bootstrap4 ( Pixel Perfect / UI, UX )

[Live Demo: https://merantau.gitlab.io/one-page-responsive/ ](https://merantau.gitlab.io/one-page-responsive/)
![Image description](./screenshot.png)

---
### Thank you, @merantau